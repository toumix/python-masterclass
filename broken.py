import numpy as np
def f(x):
    return np.round(x/(1-x))


g = lambda y: f(f(y))


def h(z):
  return f(g(z))


print(h(1))
