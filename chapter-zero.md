# Chapter Zero: Environment Setup

In this chapter, we learn how to setup our python environment properly.

## "Hello word!"

Open your favourite text editor (mine is [Atom](https://atom.io/), it's open-source, cross-platform and hackable) then create a new file `hello.py` with the two lines:
```python
from platform import python_version
print("Hello world, I am python {}!".format(python_version()))
```
Open your terminal and type the one line:
```shell
$ python hello.py
Hello world, I am python 2.7!
```
If you're lucky, then python is already installed on your machine and it should say hello to you, and telling you what version is installed.
Congrats, in three lines you have run your first python program!

If you're unlucky, python did not say hello to you and instead the terminal complained.
There are about a thousand reasons why this could be the case: maybe python is not installed at all, maybe you have an old version of python, maybe python is installed but your terminal doesn't know, etc.

In any case, in the next section we make sure that everybody is on the same page, running the same version of python.

## pyenv

We want to make sure we are running the same code in the same way, and that goes through making sure that we are running the same version of python.
Handling multiple versions and making our terminal makes the difference between them (by setting a so-called "`PATH` variable") can be a mess, but there is a tool for that: `pyenv`.

[Here](https://github.com/pyenv/pyenv-installer) are instruction for Linux, it's a simple copy and paste. For MacOS, you can first install the package manager [brew](https://brew.sh/) then run `brew install pyenv`.
For Windows it's gonna be a bit more involved, but you can find instructions [here](https://pypi.org/project/pyenv-win/).

Now we can ask `pyenv` to install a chosen version of python for us:

```shell
$ pyenv install 3.7.5
```

This should take some time to download and install everything properly, printing a lot of stuff in the process. See [here](https://github.com/pyenv/pyenv/wiki/common-build-problems) if you get any problems on the way.
Now we can check the different versions we have installed with:

```shell
$ pyenv versions
* system
3.7.5
```

The active version is preceded by a `*`. We can set `3.7.5` as our global version with:

```shell
$ pyenv global 3.7.5
```

Let's check the version has indeed been updated:

```shell
$ pyenv versions
system
* 3.7.5
```

Now we should all get the same message as the output of our simple program:

```shell
$ python hello.py
Hello world, I am python 3.7.5!
```

## pip

`pip` stands for "python install package", you get for free with `pyenv`.
If we want to build more complicated programs than our `hello.py` then we can't hope to code everything from scratch: we need external packages.
For example, we don't want to redefine matrix multiplication ourselves, instead we're going to use [numpy](https://numpy.org):

```shell
$ pip install numpy
```

We can check what packages are installed globally with:

```shell
$ pip list
Package    Version
---------- -------
numpy      1.17.4
pip        19.0.3
setuptools 40.8.0
```

You can look at a package in detail with:

```shell
$ pip show numpy
Name: numpy
Version: 1.17.4
Summary: NumPy is the fundamental package for array computing with Python.
```

## pycodestyle

Another useful package we're going to use is `pycodestyle`:

```shell
$ pip install pycodestyle
```

We can now give it our code as input:

```shell
$ pycodestyle hello.py
```

If you're writing beautiful code, `pycodestyle` shouldn't do or say anything.

If it complains, it means that you broke the style conventions of python, you can check them out [here](https://www.python.org/dev/peps/pep-0008/).
In Atom, you can install the [linter](https://atom.io/packages/linter) and [linter-pycodestyle](https://atom.io/packages/linter-pycodestyle) plugins so that you can see style warnings directly in your text editor.

```shell
$ pycodestyle broken.py
broken.py:2:1: E302 expected 2 blank lines, found 0
broken.py:6:1: E731 do not assign a lambda expression, use a def
broken.py:10:3: E111 indentation is not a multiple of four
```

## venv

It's not enough to have the same version of python, we'd better make sure that we have the same versions of each package too.
The cleanest way to do that is to set up a _virtual environment_:

```shell
$ python -m venv ENV
$ source ENV/bin/activate
```

(If you're on windows you'll need to run `ENV\Scripts\activate.bat` instead.)

You should see `(ENV)` next to your prompt: your terminal is in a virtual environment! If you do `pip list` again, you can see that `numpy` has disappeared so let's run `pip install` again:

```shell
pip install numpy
```
(It shouldn't have to download again.)

Now we can "freeze" our list of packages and the exact version for each:

```shell
$ pip freeze
numpy==1.17.4
```

If we store the output of `pip freeze` in a file `requirements.txt`, we can re-install the exact same packages on a possibly different machine:

```shell
$ pip freeze > requirements.txt
$ pip install -r requirements.txt
```

## git

Ok so now that we have our little `hello.py` experiment running on our machine, we want to share it to our friends and make sure they can run it to.

We're going to to do this with `git`. First, we're going to create a new branch in our repo so that we can all edit the same code in parallel:

```shell
$ git branch alice
```

Then we switch to our new branch:

```shell
$ git checkout alice
Switched to branch 'alice'
```

We can have a look at what's happening with:

```shell
$ git status
On branch alice
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	ENV/
	hello.py
    requirements.txt

nothing added to commit but untracked files present (use "git add" to track)
```

Now let's add, commit and push `hello.py` and `requirements.txt`:
```shell
$ git add hello.py requirements.txt
$ git commit -m "my first commit"
$ git push
```

In general you should avoid the command `git add *` which adds everything to the commit.
For example, you don't want to push your `ENV` folder.
In order to solve this, we can create a file `.gitignore` and add the two lines:

```
.*
ENV/*
```

## argparse

For now, our program `hello.py` always does the same boring stuff.
If we want more exciting programs, we need to be able to give them inputs:
`argparse` is the clean way to do just that.
It's already in the standard python library so you don't need to install anything.
Let's have a look at a simple script:

```python
from argparse import ArgumentParser
from platform import python_version


def main(args):
    print("Hello {}, I am python {}!".format(args.name, python_version()))


if __name__ == '__main__':
    parser = ArgumentParser(description='Hello!')
    parser.add_argument('--name', help='your name', default='world')
    args = parser.parse_args()
    main(args)
```

In the first lines, we do some imports then we define a simple function `main`.
The statement `if __name__ == '__main__':` is a way to make sure we can import `main` from some other module without running the next block of code by accident.
If we run:

```shell
$ python hello.py --name Alice
Hello Alice, I am python 3.7.5!
```

then python knows we are running `hello.py` as our main script, and he prints a personal greeting.
If we come back to our code in a month and forgot how to use it, we can always ask for help:

```shell
$ python hello.py -h
usage: hello.py [-h] [--name NAME]

Hello!

optional arguments:
  -h, --help   show this help message and exit
  --name NAME  your name
```

## pdb

`pdb` stands for python debugger, it's going to be our best friend as soon as we start writing more complex code.
It's a standard python module, which you can call with `python -m pdb`.
Let's take our `broken.py` script as an example:

```shell
$ python -m pdb broken.py
> /Users/aleumi/WORK/python-masterclass/broken.py(1)<module>()
-> import argparse
(Pdb)
```

Now that you're in debugging mode, you can use the command `c` to run your code:

```
(Pdb) c
Traceback (most recent call last):
  ...
  File "/Users/aleumi/WORK/python-masterclass/broken.py", line 11, in h
    return f(g(z))
  File "/Users/aleumi/WORK/python-masterclass/broken.py", line 8, in <lambda>
    g = lambda y: f(f(y))
  File "/Users/aleumi/WORK/python-masterclass/broken.py", line 6, in f
    return np.round(x/(1-x))
ZeroDivisionError: division by zero
Uncaught exception. Entering post mortem debugging
Running 'cont' or 'step' will restart the program
> /Users/aleumi/WORK/python-masterclass/broken.py(6)f()
-> return np.round(x/(1-x))
```

Things didn't turn out so well... We can use `u` and `d` to up and down the chain
of functions that caused the bug:

```
(Pdb) u
-> g = lambda y: f(f(y))
(Pdb) u
-> return f(g(z))
(Pdb) u
-> import argparse
(Pdb) d
-> return f(g(z))
```

We can check the values for any of the variables at that point in the code:
```
(Pdb) z
1
(Pdb) g(z)
*** ZeroDivisionError: division by zero
(Pdb) d
-> g = lambda y: f(f(y))
(Pdb) d
-> return np.round(x/(1-x))
(Pdb) 1-x
0
```

So now we found our bug!
