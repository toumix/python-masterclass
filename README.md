# Python data science

We learn how to do data science with Python.

## First steps

1) Make sure you have `git` installed on your machine, here are the instructions: https://www.wikihow.com/Set-Up-and-Use-Git

2) Open your terminal (here are instructions for [Windows](https://www.wikihow.com/Open-Terminal-in-Windows), [Mac OS](https://www.wikihow.com/Open-a-Terminal-Window-in-Mac) and [Ubuntu](https://www.wikihow.com/Open-a-Terminal-Window-in-Ubuntu)) and type

```shell
git clone git@gitlab.com:toumix/python-masterclass.git
```

3) You're done! Now you can go and read [chapter zero](chapter-zero.md).

## Quick start

Once you read chapter zero, you can simply run:

```shell
python -m venv ENV
source ENV/bin/activate
pip install -r requirements.txt
python hello.py
```
