# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from platform import python_version


def main(args):
    print("Hello {}, I am python {}!".format(args.name, python_version()))


if __name__ == '__main__':
    parser = ArgumentParser(description='Hello!')
    parser.add_argument('--name', help='your name', default='world')
    args = parser.parse_args()
    main(args)
